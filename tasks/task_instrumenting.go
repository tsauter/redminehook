package tasks

import (
	"context"
	"time"
)

// TaskInstrumenting implements a Prometheus instrumenting
// around regular SchedulerTask(s).
type TaskInstrumenting struct {
	Next SchedulerTask
}

// Name returns the name of the underlaying task.
func (ti *TaskInstrumenting) Name() string {
	return ti.Next.Name()
}

// Run exports metrics of the current task and executes the underlaying task.
func (ti *TaskInstrumenting) Run(ctx context.Context) error {
	defer func(begin time.Time) {
		//lvs := []string{"method", ti.Next.Name()}
		//queryLatency.WithLabelValues(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return ti.Next.Run(ctx)
}
