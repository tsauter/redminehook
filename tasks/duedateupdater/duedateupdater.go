package duedateupdater

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/tsauter/redminehook/core"
	"gitlab.com/tsauter/redminehook/redminecore"

	"github.com/pkg/errors"
	"gitlab.com/tsauter/go-redmine"
)

const (
	// our task name
	name = "DueDateUpdater"
)

// A DueDateUpdater updates the due date of Redmine issues through a
// Redmine API via JSON http requests. An API key is used for authentication.
type DueDateUpdater struct {
	m             sync.Mutex
	redmineURI    string
	projectID     uint
	dueDateDelta  uint
	redmineClient redminecore.RedmineEndpoint
}

// NewDueDateUpdater returns an initialized DueDateUpdater.
func NewDueDateUpdater(redmineEndpointURL string, redmineAPIKey string, projectID uint, dueDateDelta uint) (*DueDateUpdater, error) {
	// create the Redmine API client
	client := redmine.NewClient(redmineEndpointURL, redmineAPIKey)
	return &DueDateUpdater{redmineURI: redmineEndpointURL, redmineClient: client, projectID: projectID, dueDateDelta: dueDateDelta}, nil
}

// Name returns the name of this task.
func (du *DueDateUpdater) Name() string {
	return name
}

// Run implements the Run() function for the SchedulerTask interface.
// Under the hood, the processIssues() function is called.
func (du *DueDateUpdater) Run(ctx context.Context) error {
	du.processIssues(ctx)
	return nil
}

// processIssues search for all issues without a due date set.
// Each found issue will be extended with a due date.
func (du *DueDateUpdater) processIssues(ctx context.Context) int {
	log.Printf("[%s] query all issues", name)

	// convert configured delta to time.Duration
	duration := time.Duration(du.dueDateDelta) * time.Second

	// create a filter for the current project and tickets with all states
	// and empty due date
	filter := &redmine.IssueFilter{
		ProjectId: fmt.Sprintf("%d", du.projectID),
		StatusId:  "*",
		DueDate:   "!*",
	}

	// variables for tracking
	total := 0
	failed := 0

	// this function will be executed for each individual found issue.
	// it can access the public variables total and failed
	updateHandler := func(issue *redmine.Issue) error {
		total++

		// update the issue
		if err := du.updateIssueDueDate(*issue, duration); err != nil {
			failed++
			log.Printf("[%s] failed to update issue: %d: %v", name, issue.Id, err)
			return nil // continue on errors anyway
		}

		return nil
	}

	// query issues as long as we get less then the limit, if this is the case
	// we are at the end of the pagination
	err := du.redmineClient.IssuesByFilterAll(filter, updateHandler)
	if err != nil {
		log.Printf("[%s] failed to collect and process issues: %v", name, err)
		return total
	}

	log.Printf("[%s] check finished (total: %d, failed: %d)", name, total, failed)

	return total
}

// updateIssueDueDate takes on individual issue and set the due date. The due date is calculated
// either on the StatusDate or the CreatedOn date. The duration is added to one of these two data.
// If no validate date was found, the current UTC time will be used for calculation.
func (du *DueDateUpdater) updateIssueDueDate(issue redmine.Issue, maxDueDateDuration time.Duration) error {
	// skip all issues with already set DueDate
	if issue.DueDate != "" {
		return nil
	}

	// determe which date is our base date for the due date calculation
	// possible fields:
	// FIXME: shouldn't StatusDate to be the StartDate?
	// - StatusDate   string         `json:"status_date"`
	// - CreatedOn    string         `json:"created_on"`
	// - (current UTC time)
	var baseDate time.Time
	if issue.StatusDate != "" {
		d, err := core.ConvertRedmineShortDate(issue.StatusDate)
		if err != nil {
			return errors.Wrapf(err, "issue has invalid status date: %s", issue.StatusDate)
		}
		baseDate = d
	} else if issue.CreatedOn != "" {
		d, err := core.ConvertRedmineDate(issue.CreatedOn)
		if err != nil {
			return errors.Wrapf(err, "issue has invalid created_on: %s", issue.CreatedOn)
		}
		baseDate = d
	} else {
		baseDate = time.Now().UTC()
	}

	// add the duration to the selected date and convert this date to the Redmine
	// short date string (usually 2006-01-02).
	newDueDate := baseDate.Add(maxDueDateDuration)
	newDueDateString := newDueDate.Format(redminecore.RedmineShortDateFormat)
	log.Printf("[%s] issue: [%d] %s -> %s", name, issue.Id, issue.Subject, newDueDateString)

	// update issue by invoking the Redmine API.
	issue.DueDate = newDueDateString
	err := du.redmineClient.UpdateIssue(issue)
	if err != nil {
		return err
	}

	return nil
}
