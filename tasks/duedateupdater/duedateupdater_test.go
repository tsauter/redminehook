package duedateupdater

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"sort"
	"testing"
	"time"

	"gitlab.com/tsauter/redminehook/redminecore"

	"gitlab.com/tsauter/go-redmine"
)

const (
	// DueDateDuration is used always set the same duration during tests.
	// The result must match the date in the testresults.json file.
	DueDateDelta    = 60 * 60 * 24 * 2
	DueDateDuration = time.Duration(DueDateDelta) * time.Second
)

var (
	TestIssues  []redmine.Issue
	TestResults TestResult
)

// A ResultData is a containter to hold expected responses
// for test cases.
type ResultData struct {
	// DueDate is read from json file
	DueDate time.Time `json:"-"`
	// DueDateString is the converted string to RedmineDateString
	DueDateString string `json:"due_date"`
}

type TestResult map[int]ResultData

// init reads the test issues and test cases from
// json files in the skeleton directory.
// These test dates are used in all test cases.
func init() {
	// Read the test issues
	data, err := ioutil.ReadFile("../../skeletons/issues.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &TestIssues)
	if err != nil {
		panic(err)
	}

	// Read the test resuls
	data, err = ioutil.ReadFile("../../skeletons/testresults.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &TestResults)
	if err != nil {
		panic(err)
	}

	for i := range TestResults {
		tmp := TestResults[i]
		d, err := time.Parse(redminecore.RedmineShortDateFormat, tmp.DueDateString)
		if err != nil {
			panic(err)
		}
		tmp.DueDate = d
		TestResults[i] = tmp
	}
}

// TestProcessIssue tests the processIssue function of the DueDateUpdater.
// After invoking the function all test issues should have a new due date afterwards.
// The new DueDate must match the specified DueDate in the TestResults.
func TestProcessIssues(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	redmineMock, err := redminecore.NewRedmineMock(&TestIssues)
	if err != nil {
		t.Fatalf("failed to initialize RedmineMock: %v", err)
	}

	du, err := NewDueDateUpdater("http://localhost", "apikey", 0, DueDateDelta)
	if err != nil {
		t.Fatalf("failed to initialize DueDate updater: %v", err)
	}
	du.redmineClient = redmineMock

	// process all issues and set their due date
	du.processIssues(ctx)

	// create a sorted list of all items
	var keys []int
	for _, k := range redmineMock.Issues {
		keys = append(keys, k.Id)
	}
	sort.Ints(keys)

	// walk over each issue, load the test case issue compare the
	// issue against the test case
	for _, k := range keys {
		testCaseIssue := TestResults[redmineMock.Issues[k].Id]

		// reload the updated issue
		newIssue, err := redmineMock.Issue(redmineMock.Issues[k].Id)
		if err != nil {
			t.Errorf("reloading issue failed: %d: %v", redmineMock.Issues[k].Id, err)
			continue
		}

		// validate the date: "2016-01-02" == "2016-01-02"
		if newIssue.DueDate != testCaseIssue.DueDate.Format(redminecore.RedmineShortDateFormat) {
			t.Errorf("new due date is invalid: %d: %s (expected %s)", redmineMock.Issues[k].Id, newIssue.DueDate, testCaseIssue.DueDate.Format(redminecore.RedmineShortDateFormat))
			continue
		}

		t.Logf("issue updated: %d: %s", redmineMock.Issues[k].Id, newIssue.DueDate)
	}
}

// TestUpdateIssueDueDate tests the updateIssue function of the DueDateUpdater.
// By updating each issue with DueDateDuration and then comparing the result
// against the TestResults, the updateIssue function can be validated.
// The new DueDate must match the specified DueDate in the TestResults.
func TestUpdateIssueDueDate(t *testing.T) {
	// initialize the Redmine client mock
	redmineMock, err := redminecore.NewRedmineMock(&TestIssues)
	if err != nil {
		t.Fatalf("failed to initialize RedmineMock: %v", err)
	}

	// create a new DueDateUpdater, the parameters do not matter here
	du, err := NewDueDateUpdater("http://localhost", "apikey", 0, DueDateDelta)
	if err != nil {
		t.Fatalf("failed to initialize DueDate updater: %v", err)
	}
	du.redmineClient = redmineMock

	// create a sorted list of all items
	var keys []int
	for _, k := range redmineMock.Issues {
		keys = append(keys, k.Id)
	}
	sort.Ints(keys)

	// walk over each issue, load the test case issue,
	// update the issue and reload it and validate the reloaded
	// issue against the test case
	for _, k := range keys {
		testCaseIssue := TestResults[redmineMock.Issues[k].Id]

		// update the issue
		err = du.updateIssueDueDate(redmineMock.Issues[k], DueDateDuration)
		if err != nil {
			t.Errorf("updating issue failed: %d: %v", redmineMock.Issues[k].Id, err)
			continue
		}

		// reload the updated issue
		newIssue, err := redmineMock.Issue(redmineMock.Issues[k].Id)
		if err != nil {
			t.Errorf("reloading issue failed: %d: %v", redmineMock.Issues[k].Id, err)
			continue
		}

		// validate the date: "2016-01-02" == "2016-01-02"
		if newIssue.DueDate != testCaseIssue.DueDate.Format(redminecore.RedmineShortDateFormat) {
			t.Errorf("new due date is invalid: %d: %s (expected %s)", redmineMock.Issues[k].Id, newIssue.DueDate, testCaseIssue.DueDate.Format(redminecore.RedmineShortDateFormat))
			continue
		}

		t.Logf("issue updated: %d: %s", redmineMock.Issues[k].Id, newIssue.DueDate)
	}

}
