package tasks

import (
	"context"
	"log"
)

// TaskLogger implements logging functions around
// regular SchedulerTask(s).
type TaskLogger struct {
	Next SchedulerTask
}

// Name returns the name of the underlaying task.
func (tl *TaskLogger) Name() string {
	return tl.Next.Name()
}

// Run logs the current task and executes the underlaying task.
func (tl *TaskLogger) Run(ctx context.Context) error {
	log.Printf("[%s] Executing task job...", tl.Next.Name())
	defer log.Printf("[%s] Task finished", tl.Next.Name())
	return tl.Next.Run(ctx)
}
