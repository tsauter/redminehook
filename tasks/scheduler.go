package tasks

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"
)

// Scheduler implements a basic dispatcher for tasks.
// Each task is simply a go routine that will do something
// in the background
type Scheduler struct {
	m             sync.Mutex
	checkInterval uint
	tasks         []SchedulerTask
}

// NewScheduler initialize a new scheduler.
func NewScheduler(interval uint) (*Scheduler, error) {
	return &Scheduler{checkInterval: interval}, nil
}

// RegisterTask adds the task to the task queue of the
// scheduler. All tasks in this queue will be started later.
func (s *Scheduler) RegisterTask(task SchedulerTask) error {
	s.m.Lock()
	defer s.m.Unlock()

	var taskChain SchedulerTask
	taskChain = task
	taskChain = &TaskInstrumenting{Next: task}
	taskChain = &TaskLogger{Next: taskChain}

	log.Printf("Registering task %s", task.Name())
	s.tasks = append(s.tasks, taskChain)
	return nil
}

// StartTasks starts each previously registered task.
// The starting will be done parallel.
func (s *Scheduler) StartTasks(ctx context.Context) error {
	if len(s.tasks) < 1 {
		return fmt.Errorf("no tasks registered, nothing todo")
	}

	for _, task := range s.tasks {
		go func(t SchedulerTask) {
			log.Printf("Spinning task %s", t.Name())
			s.startTask(ctx, t)
		}(task)
	}
	return nil
}

func (s *Scheduler) startTask(ctx context.Context, task SchedulerTask) {
	ticker := time.NewTicker(time.Second * time.Duration(s.checkInterval))
	log.Printf("[%s] interval is %ds", task.Name(), s.checkInterval)

	// immediately start the job, we do not wont to wait for the first
	// ticker event
	task.Run(ctx)

	// go function with a forever loop that wait for ticker or cancel events
	go func() {
		for {
			select {
			case <-ticker.C:
				task.Run(ctx)
			case <-ctx.Done():
				return
			}
		}
	}()

	return
}
