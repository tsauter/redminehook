package msteamsposter

import (
	"gitlab.com/tsauter/go-msteams"
)

// A MSTeamsMock can be used to simulate an MS-Teams client.
// No real data will be send to the outside world.
type MSTeamsMock struct {
	lastData []msteams.TeamsData
}

// SendToWebhook takes a TeamsDate{} variable and store this variable
// in the struct.
// This data can retreived later and validated.
func (tm *MSTeamsMock) SendToWebhook(data *msteams.TeamsData) error {
	tm.lastData = append(tm.lastData, *data)
	return nil
}
