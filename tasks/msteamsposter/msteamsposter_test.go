package msteamsposter

import (
	"context"
	"reflect"
	"testing"
	"time"

	"gitlab.com/tsauter/redminehook/redminecore"

	"gitlab.com/tsauter/go-msteams"
	"gitlab.com/tsauter/go-redmine"
)

var (
	// test cases and expected Teams responses
	testCases = []struct {
		issue     redmine.Issue
		teamsData msteams.TeamsData
	}{
		// test case #1
		{
			redmine.Issue{
				Id:          1,
				Subject:     "test issue #1",
				Description: "I have a big issue with your issue #1.",
				CreatedOn:   "2017-10-10T09:00:00Z",
				Author:      &redmine.IdName{Id: 1, Name: "Max Mustermann"},
				Project:     &redmine.IdName{Id: 1, Name: "Test-Project"},
				Priority:    &redmine.IdName{Id: 3, Name: "Medium"},
				Status:      &redmine.IdName{Id: 1, Name: "New"},
			},
			msteams.TeamsData{
				Type:       "MessageCard",
				Context:    "http://schema.org/extensions",
				Summary:    "test issue #1",
				ThemeColor: "",
				Sections: []msteams.Group{
					msteams.Group{
						StartGroup:       true,
						Title:            "**test issue #1** [#1]",
						ActivityTitle:    "Reported by **Max Mustermann**",
						ActivitySubtitle: "",
						Facts: []msteams.Fact{
							msteams.Fact{
								Name:  "Link",
								Value: "[open issue in browser](http://example.org/issues/1)",
							},
							msteams.Fact{
								Name:  "Project",
								Value: "Test-Project",
							},
							msteams.Fact{
								Name:  "Priority",
								Value: "Medium",
							},
							msteams.Fact{
								Name:  "Status",
								Value: "New",
							},
						},
					},
					msteams.Group{
						StartGroup:       false,
						Title:            "",
						ActivityTitle:    "",
						ActivitySubtitle: "I have a big issue with your issue #1.",
						Facts:            []msteams.Fact(nil),
					},
				},
			},
		},
		// test case #2
		{
			redmine.Issue{
				Id:          2,
				Subject:     "test issue #2",
				Description: "I have a big issue with your issue #2.",
				CreatedOn:   "2017-10-10T09:10:00Z",
				Author:      &redmine.IdName{Id: 1, Name: "Doe, John"},
				Project:     &redmine.IdName{Id: 1, Name: "MyProject"},
				Priority:    &redmine.IdName{Id: 3, Name: "High"},
				Status:      &redmine.IdName{Id: 1, Name: "Done"},
			},
			msteams.TeamsData{
				Type:       "MessageCard",
				Context:    "http://schema.org/extensions",
				Summary:    "test issue #2",
				ThemeColor: "",
				Sections: []msteams.Group{
					msteams.Group{
						StartGroup:       true,
						Title:            "**test issue #2** [#2]",
						ActivityTitle:    "Reported by **Doe, John**",
						ActivitySubtitle: "",
						Facts: []msteams.Fact{
							msteams.Fact{
								Name:  "Link",
								Value: "[open issue in browser](http://example.org/issues/2)",
							},
							msteams.Fact{
								Name:  "Project",
								Value: "MyProject",
							},
							msteams.Fact{
								Name:  "Priority",
								Value: "High",
							},
							msteams.Fact{
								Name:  "Status",
								Value: "Done",
							},
						},
					},
					msteams.Group{
						StartGroup:       false,
						Title:            "",
						ActivityTitle:    "",
						ActivitySubtitle: "I have a big issue with your issue #2.",
						Facts:            []msteams.Fact(nil),
					},
				},
			},
		},
	}
)

// TestPopulateToMSTeams tests the populateToMSTeams function.
// We do this by passing our test cases to this function and validate
// the returned/processed issue.
// To create better tests, we fake to current local time during the test run.
// By shifting beteen times, we can receive different issues, based on this time.
// This tests the "process only newer issues" behaviour.
func TestPopulateToMSTeams(t *testing.T) {
	// initialize the Redmine client mock
	var issues []redmine.Issue
	for _, issue := range testCases {
		issues = append(issues, issue.issue)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// function to process the test case, the function helps us
	// to separate the cases, and always provides fresh initialized variables/structs.
	testFunc := func(name string, fakeTime time.Time, expectedTeamsData *[]msteams.TeamsData) {
		// fake Redmine mock
		redmineMock, err := redminecore.NewRedmineMock(&issues)
		if err != nil {
			t.Fatalf("failed to initialize RedmineMock: %s: %v", name, err)
		}
		teamsMock := &MSTeamsMock{}

		// create the MSTeamsPoster, parameters do not count
		poster, err := NewMSTeamsPoster("http://example.org", "aaaaaaaaaaaaaaaaaa", 0, teamsMock)
		if err != nil {
			t.Fatalf("failed to created MSTeamsPoster: %v", err)
		}
		poster.redmineClient = redmineMock

		// fake the current time, and process the issues
		poster.lastRunUTC = fakeTime // fake the local time
		poster.populateToMSTeams(ctx)
		// compare the returned issues with the expectedIssues
		if expectedTeamsData == nil {
			if teamsMock.lastData != nil {
				t.Errorf("msteams data is not empty: %s: %#v", name, teamsMock.lastData)
			}
		} else if !reflect.DeepEqual(*expectedTeamsData, teamsMock.lastData) {
			t.Errorf("msteams data is different: %s: %#v", name, teamsMock.lastData)
		}
	}

	// set the run time at the beginning of the two, the function should process ll
	testFunc(
		"case1",
		time.Date(2017, 10, 10, 8, 5, 0, 0, time.UTC),
		&[]msteams.TeamsData{testCases[0].teamsData, testCases[1].teamsData},
	)

	// set the run time in the middle of the two, the function should process the second item
	testFunc(
		"case1",
		time.Date(2017, 10, 10, 9, 5, 0, 0, time.UTC),
		&[]msteams.TeamsData{testCases[1].teamsData},
	)

	// set the run time to the end of the two, the function should process no issues
	testFunc(
		"case1",
		time.Date(2017, 10, 10, 9, 15, 0, 0, time.UTC),
		nil,
	)
}

// TestSendIssueToTeams tests the sendIssueToTeams function.
// By using a teams client mock, we are able to pass in an issue,
// receive the generate teams post payload and verify this payload
// against the expected payload. If the content is different,
// something is wrong.
func TestSendIssueToTeams(t *testing.T) {
	teamsMock := &MSTeamsMock{}

	// create the MSTeamsPoster, parameters do not count
	poster, err := NewMSTeamsPoster("http://example.org", "aaaaaaaaaaaaaaaaaa", 0, teamsMock)
	if err != nil {
		t.Fatalf("failed to created MSTeamsPoster: %v", err)
	}

	// loop over each issue, pass it to the sendIssueToTeams() function
	// and validate the generated contents from the client mock
	for i, tc := range testCases {
		err = poster.sendIssueToTeams(&tc.issue)
		if err != nil {
			t.Errorf("failed to send issue: %d: %v", tc.issue.Id, err)
		}

		if !reflect.DeepEqual(tc.teamsData, teamsMock.lastData[i]) {
			t.Errorf("msteams data is different: %#v", teamsMock.lastData)
		}
	}
}
