package msteamsposter

import (
	"reflect"
	"testing"

	"gitlab.com/tsauter/go-msteams"
)

// TestSendToWebhook tests the SendToWebhook() function.
// Processing each test case and validate that the lastData content
// is always the same.
func TestSendToWebhook(t *testing.T) {
	testCases := []msteams.TeamsData{
		msteams.TeamsData{
			Type:       "MessageCard",
			Context:    "http://schema.org/extensions",
			Summary:    "test issue #1",
			ThemeColor: "",
			Sections: []msteams.Group{
				msteams.Group{
					StartGroup:       true,
					Title:            "**test issue #1** [#1]",
					ActivityTitle:    "Reported by **Max Mustermann**",
					ActivitySubtitle: "",
					Facts: []msteams.Fact{
						msteams.Fact{
							Name:  "Link",
							Value: "[open issue in browser](http://example.org/issues/1)",
						},
						msteams.Fact{
							Name:  "Project",
							Value: "Test-Project",
						},
						msteams.Fact{
							Name:  "Priority",
							Value: "Medium",
						},
						msteams.Fact{
							Name:  "Status",
							Value: "New",
						},
					},
				},
				msteams.Group{
					StartGroup:       false,
					Title:            "",
					ActivityTitle:    "",
					ActivitySubtitle: "I have a big issue with your issue #1.",
					Facts:            []msteams.Fact(nil),
				},
			},
		},
		msteams.TeamsData{
			Type:       "MessageCard",
			Context:    "http://schema.org/extensions",
			Summary:    "test issue #1",
			ThemeColor: "",
			Sections: []msteams.Group{
				msteams.Group{
					StartGroup:       false,
					Title:            "**test issue #2** [#2]",
					ActivityTitle:    "Reported by **ME**",
					ActivitySubtitle: "",
					Facts: []msteams.Fact{
						msteams.Fact{
							Name:  "Link",
							Value: "[open issue in browser](http://example.org/issues/2)",
						},
						msteams.Fact{
							Name:  "Project",
							Value: "Test-Project",
						},
						msteams.Fact{
							Name:  "Priority",
							Value: "Low",
						},
						msteams.Fact{
							Name:  "Status",
							Value: "Done",
						},
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		// create a new mock each time, to reset the internal data
		// otherwise we would have to track the position
		mock := MSTeamsMock{}

		err := mock.SendToWebhook(&tc)
		if err != nil {
			t.Fatalf("failed to SendToWebhook: %v", err)
		}

		if !reflect.DeepEqual(tc, mock.lastData[0]) {
			t.Errorf("lastData is differnt: %#v", mock.lastData)
		}
	}
}
