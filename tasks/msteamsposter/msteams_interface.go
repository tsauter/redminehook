package msteamsposter

import (
	"gitlab.com/tsauter/go-msteams"
)

// A MSTeamsClient is a client than sends TeamsData{} to
// a MS-Teams HTTP-Rest-API.
type MSTeamsClient interface {
	SendToWebhook(*msteams.TeamsData) error
}
