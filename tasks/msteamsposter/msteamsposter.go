package msteamsposter

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/tsauter/redminehook/redminecore"

	"gitlab.com/tsauter/go-msteams"
	"gitlab.com/tsauter/go-redmine"
)

const (
	// our task name
	name = "MSTeamsPoster"
)

// A MSTeamsPoster seaches for new Redmine issues inside a project
// and send a formated MS Teams post for each issue.
// Only "new" issues since our last run are considered. We do not
// wan't to push all found issue. The last run will be stored non-persistent
// in the lastRunUTC in our structure. There might be caps during downtimes
// of this task.
type MSTeamsPoster struct {
	m             sync.Mutex
	redmineURI    string
	projectID     uint
	redmineClient redminecore.RedmineEndpoint
	lastRunUTC    time.Time
	teamsHook     MSTeamsClient
}

// NewMSTeamsPoster returns an initialized MSTeamsPoster.
func NewMSTeamsPoster(redmineEndpointURL string, redmineAPIkey string, projectID uint, teamsClient MSTeamsClient) (*MSTeamsPoster, error) {
	// the Redmine API client
	client := redmine.NewClient(redmineEndpointURL, redmineAPIkey)
	return &MSTeamsPoster{redmineURI: redmineEndpointURL, projectID: projectID, redmineClient: client, lastRunUTC: time.Now().UTC(), teamsHook: teamsClient}, nil
}

// Name returns the name of this task.
func (tp *MSTeamsPoster) Name() string {
	return name
}

// Run implements the Run() function for the SchedulerTask interface.
// Under the hood, the populateToMSTeams() function is called.
func (tp *MSTeamsPoster) Run(ctx context.Context) error {
	tp.populateToMSTeams(ctx)
	return nil
}

// populateToMSTeams query Redmine for new issues and push
// each detected issue to MS-Teams.
// Only "new" issues since our last run are considered. We
// do not wan't to push all found issue.
// The last run will be stored non-persistent in the lastRunUTC in our structure.
func (tp *MSTeamsPoster) populateToMSTeams(ctx context.Context) {
	log.Printf("[%s] check for new issues since %s", name, tp.lastRunUTC)

	// create a filter for the current project and tickets with all states
	// the filter limits issues that are created 5 minutes before or after the last run
	nowMinus5Min := tp.lastRunUTC.Truncate(time.Second * 60 * 5)
	filter := &redmine.IssueFilter{
		ProjectId: fmt.Sprintf("%d", tp.projectID),
		StatusId:  "*",
		CreatedOn: &redmine.DateFilterOption{Operator: redmine.GreaterOrEqual, Value: nowMinus5Min},
	}

	// after the end of the function, store the last run time
	// we use this time to filter new issues during the *next* cycle!
	//log.Printf("[msteams] new run should check after %s", lastRun)
	thisRun := time.Now().UTC()
	defer func() {
		tp.m.Lock()
		defer tp.m.Unlock()
		tp.lastRunUTC = thisRun
	}()

	// walk over each issue, filter all issue which are created after(!) our last run
	// and send them to MS teams (through a Teams-Webhook)
	total := 0
	considered := 0
	failed := 0

	// issueHandler is a function to process each individual issue
	// the function is definied inside if populateToMSTeams to be able to
	// access internal variables (total, considered, failed, ...)
	issueHandler := func(issue *redmine.Issue) error {
		total++

		// parse the issue date from string to a go time
		issueDate, err := time.Parse("2006-01-02T15:04:05Z", issue.CreatedOn)
		if err != nil {
			log.Printf("[%s] failed to parse date/time string: %d: %v", name, issue.Id, err)
			return nil
		}

		// skip all older entries, we only process issue that are
		// created *after* out last run
		if issueDate.Before(tp.lastRunUTC) {
			return nil
		}

		//fmt.Printf("%d %s %s -> %s\n", issue.Id, issue.Subject, issue.Status, issue.CreatedOn, issuedate)
		//fmt.Printf("%s -> %s\n", issue.CreatedOn, issueDate)

		log.Printf("[%s] new issue: [%d] %s", name, issue.Id, issue.Subject)
		considered++

		// invoke webhook
		err = tp.sendIssueToTeams(issue)
		if err != nil {
			failed++
			log.Printf("[%s] failed to send issue to MS-Teams: %d: %v", name, issue.Id, err)
		}
		return nil
	}

	// query for all issues and pass each issue to the issueHandler() function
	err := tp.redmineClient.IssuesByFilterAll(filter, issueHandler)
	if err != nil {
		log.Printf("[%s] failed to collect and process issues: %v", name, err)
		return
	}

	log.Printf("[%s] check finished (total: %d, considered: %d, failed: %d)",
		name, total, considered, failed)

	return
}

// sendIssueToTeams takes a Redmine issue, build a MS Teams
// JSON object and forward them to the MS-Teams webhook.
// The layout of the teams post is definied through the structure
// of the JSON object.
func (tp *MSTeamsPoster) sendIssueToTeams(issue *redmine.Issue) error {
	// the main Teams struct
	data, err := msteams.NewTeamsData(issue.Subject)
	if err != nil {
		return fmt.Errorf("failed to create data for Teams: %v", err)
	}

	// append a new group
	g := msteams.Group{
		StartGroup:    true,
		Title:         fmt.Sprintf("**%s** [#%d]", issue.Subject, issue.Id),
		ActivityTitle: fmt.Sprintf("Reported by **%s**", issue.Author.Name),
		Facts: []msteams.Fact{
			{Name: "Link", Value: fmt.Sprintf("[open issue in browser](%s/issues/%d)", tp.redmineURI, issue.Id)},
			{Name: "Project", Value: issue.Project.Name},
			{Name: "Priority", Value: issue.Priority.Name},
			{Name: "Status", Value: issue.Status.Name},
		},
	}
	data.AddSectionGroup(g)

	// append a new group (if we have a description)
	if len(issue.Description) > 0 {
		g = msteams.Group{
			StartGroup:       false,
			ActivitySubtitle: issue.Description,
		}
		data.AddSectionGroup(g)
	}

	// forwared the completed object to the Teams client
	// usually this invokes a http post request.
	err = tp.teamsHook.SendToWebhook(data)
	if err != nil {
		return fmt.Errorf("failed to post webhook: %v", err)
	}

	return nil
}
