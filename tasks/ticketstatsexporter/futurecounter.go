package ticketstatsexporter

import (
	"time"
)

// A FutureCounter represents a specify day in the future.
type FutureCounter struct {
	Name      string
	FutureDay time.Time
}

// NewFutureCounter returns a FutureCounter with a time in the future.
// The name is a simple text representation of the FutureDay.
// The calculated time depends on the current time and a number of days added.
func NewFutureCounter(name string, plusDays int) FutureCounter {
	now := time.Now().UTC().Add(time.Hour * 24 * time.Duration(plusDays))
	fday := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	return FutureCounter{name, fday}
}
