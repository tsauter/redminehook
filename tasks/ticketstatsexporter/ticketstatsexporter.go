package ticketstatsexporter

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/tsauter/redminehook/core"
	"gitlab.com/tsauter/redminehook/redminecore"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/tsauter/go-redmine"
)

const (
	// our task name
	name = "TicketStatsExporter"
)

var (
	// redmineTicketStats is the Prometheus gauge metric
	// which is used to store and export the counted issue states
	// all issues are reported to this metric divided by the metric vectores
	redmineTicketStats = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "redmine_issues",
			Help: "Number of Redmine issue with a specific state.",
		},
		[]string{"endpoint", "project", "status"},
	)
)

// init is executed on program startup, and only once
func init() {
	// Initialize the used Prometheus metric variables
	prometheus.MustRegister(redmineTicketStats)
}

// A TicketStatsExporter provides various metrics for Redmine issues
// with the help of the Prometheus export method.
// The web server, that provides the metrics through HTTP, is not part of
// this exporter. The server must be started within the main program.
type TicketStatsExporter struct {
	redmineURI    string
	projectID     uint
	redmineClient redminecore.RedmineEndpoint
	closedStates  []string
}

// IssueCounter stores the cumulated numbers of the different issue states.
// The name of the state is the key.
// The IssueCounter can be used to count multiple issues and keep their states.
type IssueCounter map[string]int

// NewTicketStatsExporter returns a new TicketStatsExporter
func NewTicketStatsExporter(redmineEndpointURL string, redmineAPIKey string, projectID uint, closedStates []string) (*TicketStatsExporter, error) {
	// create the Redmine API client
	client := redmine.NewClient(redmineEndpointURL, redmineAPIKey)
	return &TicketStatsExporter{redmineURI: redmineEndpointURL, redmineClient: client, projectID: projectID, closedStates: closedStates}, nil
}

// Name returns the name of this tasks.
func (tse *TicketStatsExporter) Name() string {
	return name
}

// Run implements the Run() function for the SchedulerTask interface.
// Under the hood, the updatePrometheusTicketStats() function is called.
func (tse *TicketStatsExporter) Run(ctx context.Context) error {
	tse.updatePrometheusTicketStats(ctx)
	return nil
}

// updatePrometheusTicketStats query Redmine for new issues and counts the state
// of each ticket. The numbers will be stored in one states-map so we will have
// a total for all issues.
func (tse *TicketStatsExporter) updatePrometheusTicketStats(ctx context.Context) *IssueCounter {
	log.Printf("[%s] check for new issues", name)

	// create a filter to query *all* issues of the specified project
	filter := &redmine.IssueFilter{ProjectId: fmt.Sprintf("%d", tse.projectID), StatusId: "*"}

	// create our FutureCounts
	// we cannot do this during initialization, during the creation the current date
	// is used to calculate the days
	futureDays := []FutureCounter{
		NewFutureCounter("_openDueToday", 1),
		NewFutureCounter("_openDueThreeDays", 3),
	}

	// initialize the issue counter
	totals := IssueCounter{}

	// helper funtion which can be passed to IssuesByFilterAll() to process each issue
	// to make storing of all items in the "totals" variable possible,
	// we create the function inside(!) of this function
	updateHandler := func(issue *redmine.Issue) error {
		//fmt.Printf("%#v\n", issue)
		tse.countIssue(issue, &totals, &futureDays)
		return nil
	}

	// query all issues, and process each returned issue through the updateHandler() function
	err := tse.redmineClient.IssuesByFilterAll(filter, updateHandler)
	if err != nil {
		log.Printf("[%s] failed to collect and process issues: %v", name, err)
		return nil
	}

	// loop of all found states and pass them to the Prometheus metric counter
	for key, value := range totals {
		redmineTicketStats.With(prometheus.Labels{"endpoint": tse.redmineURI, "project": fmt.Sprintf("%d", tse.projectID), "status": key}).Set(float64(value))
	}

	log.Printf("[%s] check finished (_total: %d, _open: %d, _openDueToday: %d)", name, totals["_total"], totals["_open"], totals["_openDueToday"])

	return &totals
}

// countIssue processes an issue and increment various counters based on the issue
// values.
// The following counters will be modified:
//  - _total: a counter which collects all processed issues
//  - "Status": each status will be counted separatly with this counter ("New", "Done", "InProgress", ...)
// - _closed: counts all closed issues (this means all states which are in the closedStates slice)
// - _open: counts all open issues (this means everything that is not closed)
// - "futureDay": dynamic future day counters, if the match the FutureDay, the counter will be increased; based on the DueDate field
func (tse *TicketStatsExporter) countIssue(issue *redmine.Issue, counter *IssueCounter, futureDays *[]FutureCounter) error {
	(*counter)["_total"]++

	// count issues with a valid Status set
	// if we have no valid status we treat the status as open
	if (issue.Status != nil) && (issue.Status.Name != "") {
		if strings.HasPrefix(issue.Status.Name, "_") {
			return fmt.Errorf("status names with underscore are not allowed: %s", issue.Status.Name)
		}

		(*counter)[issue.Status.Name]++

		// skip all closed issues now, all further processing only collects non-closed issues
		// what "closed" means depends on which states are included in the []closedStates slice.
		if core.InSlice(tse.closedStates, issue.Status.Name) {
			(*counter)["_closed"]++
			return nil
		}
	}

	// increment the _open counter, we alreadys skipped all closed states
	// with other words, the current state must be an open state
	(*counter)["_open"]++

	// skip all issues without a DueDate set
	// all further processing is based on a set and valid due date
	if issue.DueDate == "" {
		return nil
	}

	// parse the Redmine date to a time struct, this is required to compare the
	// due dates with real dates later
	issueDate, err := time.Parse("2006-01-02", issue.DueDate)
	if err != nil {
		return fmt.Errorf("failed to parse date/time string: %d: %v", issue.Id, err)
	}

	// process each passed in FutureDays elements
	// if the issue DueDate is before or on the same day of the FutureDays element
	// this issue will be counted unter the name of this FutureDay element
	// E.g.
	//  - today is 2018-03-01
	//  - FutureDay is: _openDueToday / 2018-03-10
	//  - issue DueDate is 2018-03-09 or 2018-03-10
	//  => this issue will be counter unter "_openDueToday"
	for _, futureDay := range *futureDays {
		//fmt.Printf("FD:    %s\n", futureDay.FutureDay)
		//fmt.Printf("Due:   %s\n", issueDate)
		if issueDate.Before(futureDay.FutureDay) || issueDate.Equal(futureDay.FutureDay) {
			//fmt.Printf("       %s\n", futureDay.Name)
			(*counter)[futureDay.Name]++
		}
	}

	return nil
}
