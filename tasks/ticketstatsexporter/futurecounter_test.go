package ticketstatsexporter

import (
	"testing"
	"time"
)

// TestGetFutureDay tests the GetFutureDay function.
// During the test, the expected time will be calculated and compared
// against the time which was returned by the tested function.
func TestGetFutureDay(t *testing.T) {
	testCases := []struct {
		name string
		no   int
	}{
		{"three", 3},
		{"one", 1},
		{"thirthyseven", 37},
		{"twentyone", 21},
		{"threehundredseventyeight", 378},
	}

	for _, tc := range testCases {
		// create the future counter based on the test case
		counter := NewFutureCounter(tc.name, tc.no)

		// our own calculation, should return the same time
		testTime := func() time.Time {
			now := time.Now().UTC().Add(time.Hour * 24 * time.Duration(tc.no))
			return time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		}()

		// comparison of both names
		if tc.name != counter.Name {
			t.Errorf("invalid name returned: %s", tc.name)
		}

		// comparison of both times
		if testTime != counter.FutureDay {
			t.Errorf("invalid time returned: %d: %s", tc.no, counter.FutureDay)
		}
	}
}
