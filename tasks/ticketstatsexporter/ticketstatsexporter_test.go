package ticketstatsexporter

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"
	"time"

	"gitlab.com/tsauter/redminehook/redminecore"

	"gitlab.com/tsauter/go-redmine"
)

var (
	newState        = &redmine.IdName{Id: 1, Name: "New"}
	openState       = &redmine.IdName{Id: 2, Name: "Open"}
	inProgressState = &redmine.IdName{Id: 3, Name: "InProgress"}
	acceptedState   = &redmine.IdName{Id: 4, Name: "Accepted"}
	readyState      = &redmine.IdName{Id: 5, Name: "Ready"}
	doneState       = &redmine.IdName{Id: 6, Name: "Done"}
	rejectedState   = &redmine.IdName{Id: 7, Name: "Rejected"}

	defaultClosedStates = []string{"Done", "Rejected"}

	TestIssues  []redmine.Issue
	TestResults TestResult
)

// A ResultData is a containter to hold expected responses
// for test cases.
type ResultData struct {
	// DueDate is read from json file
	DueDate time.Time `json:"-"`
	// DueDateString is the converted string to RedmineDateString
	DueDateString string `json:"due_date"`
}

type TestResult map[int]ResultData

// init reads the test issues and test cases from
// json files in the skeleton directory.
// These test dates are used in all test cases.
func init() {
	// Read the test issues
	data, err := ioutil.ReadFile("../../skeletons/issues.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &TestIssues)
	if err != nil {
		panic(err)
	}

	// Read the test resuls
	data, err = ioutil.ReadFile("../../skeletons/testresults.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &TestResults)
	if err != nil {
		panic(err)
	}

	for i := range TestResults {
		tmp := TestResults[i]
		d, err := time.Parse(redminecore.RedmineShortDateFormat, tmp.DueDateString)
		if err != nil {
			panic(err)
		}
		tmp.DueDate = d
		TestResults[i] = tmp
	}
}

func TestUpdatePrometheusTicketStats(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	now := time.Now().UTC()

	// a set of test issues with a minimum required data
	// AV states: newState, openState, inProgressState, acceptedState, readyState, doneState, rejectedState
	issues := []redmine.Issue{
		{
			Id:      0,
			Subject: "Issue #0 - open",
			Status:  openState,
		},
		{
			Id:      1,
			Subject: "Issue #1 - done",
			Status:  doneState,
		},
		{
			Id:      2,
			Subject: "Issue #2 - new",
			Status:  newState,
		},
		{
			Id:      3,
			Subject: "Issue #3 - inProgress",
			Status:  inProgressState,
		},
		{
			Id:      4,
			Subject: "Issue #4 - accepted",
			Status:  acceptedState,
			DueDate: now.Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      5,
			Subject: "Issue #5 - done",
			Status:  doneState,
			DueDate: now.Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      6,
			Subject: "Issue #6 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 1).Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      7,
			Subject: "Issue #7 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 2).Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      8,
			Subject: "Issue #8 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 3).Format(redminecore.RedmineShortDateFormat),
		},
	}

	redmineMock, err := redminecore.NewRedmineMock(&issues)
	if err != nil {
		t.Fatalf("failed to initialize RedmineMock: %v", err)
	}

	exporter, err := NewTicketStatsExporter("http://localhost/", "nope", 1, defaultClosedStates)
	if err != nil {
		t.Fatal(err)
	}
	exporter.redmineClient = redmineMock

	var totals *IssueCounter
	totals = exporter.updatePrometheusTicketStats(ctx)
	expected := IssueCounter{
		"_total":            9,
		"_closed":           2,
		"_open":             7,
		"_openDueToday":     2,
		"_openDueThreeDays": 4,
		"Open":              1,
		"New":               1,
		"InProgress":        1,
		"Accepted":          4,
		"Done":              2,
	}
	if !reflect.DeepEqual(*totals, expected) {
		t.Errorf("countIssue returned invalid counters (total): %#v", totals)
	}
}

// TestCountIssue tests the countIssue() function of the ticketstatsexporter.
// A set of test cases will be passed to the function and the calculated status
// counts will validated then.
// First, we validate each test case individually to make sure that we receive the
// expected values from the function.
// In a second round, we loop over all issues, and validate the final result. This make
// sure we always handler the passed in pointer correct.
func TestCountIssue(t *testing.T) {
	now := time.Now().UTC()

	// a set of test issues with a minimum required data
	// AV states: newState, openState, inProgressState, acceptedState, readyState, doneState, rejectedState
	issues := []redmine.Issue{
		{
			Id:      0,
			Subject: "Issue #0 - open",
			Status:  openState,
		},
		{
			Id:      1,
			Subject: "Issue #1 - done",
			Status:  doneState,
		},
		{
			Id:      2,
			Subject: "Issue #2 - new",
			Status:  newState,
		},
		{
			Id:      3,
			Subject: "Issue #3 - inProgress",
			Status:  inProgressState,
		},
		{
			Id:      4,
			Subject: "Issue #4 - accepted",
			Status:  acceptedState,
			DueDate: now.Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      5,
			Subject: "Issue #5 - done",
			Status:  doneState,
			DueDate: now.Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      6,
			Subject: "Issue #6 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 1).Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      7,
			Subject: "Issue #7 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 2).Format(redminecore.RedmineShortDateFormat),
		},
		{
			Id:      8,
			Subject: "Issue #8 - accepted",
			Status:  acceptedState,
			DueDate: now.Add(time.Hour * 24 * 3).Format(redminecore.RedmineShortDateFormat),
		},
	}

	// a set of dates, that can be used to specific additional states with
	futureDays := []FutureCounter{
		NewFutureCounter("_openDueToday", 1),
		NewFutureCounter("_openDueThreeDays", 3),
	}

	// create the exporeter, the paramaters do not count here
	exporter, err := NewTicketStatsExporter("http://localhost/", "nope", 1, defaultClosedStates)
	if err != nil {
		t.Fatal(err)
	}

	// a helper function to process a single issue and validate their counts
	testIssue := func(issuePos int, expected IssueCounter) {
		totals := IssueCounter{}
		err = exporter.countIssue(&issues[issuePos], &totals, &futureDays)
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(totals, expected) {
			t.Errorf("countIssue returned invalid counters: %d: %#v", issuePos, totals)
		}
	}

	// validate single issue, to make sure that the returned values
	// are correct and not overwritten by other issues
	// the result depends on the defaultClosedStates variable!
	// order of the map elements does not count for validaton
	testIssue(0, IssueCounter{"_total": 1, "Open": 1, "_open": 1})
	testIssue(1, IssueCounter{"_total": 1, "Done": 1, "_closed": 1})
	testIssue(2, IssueCounter{"_total": 1, "New": 1, "_open": 1})
	testIssue(3, IssueCounter{"_total": 1, "InProgress": 1, "_open": 1})
	testIssue(4, IssueCounter{"_total": 1, "Accepted": 1, "_open": 1, "_openDueToday": 1, "_openDueThreeDays": 1})
	testIssue(5, IssueCounter{"_total": 1, "Done": 1, "_closed": 1})
	testIssue(6, IssueCounter{"_total": 1, "Accepted": 1, "_open": 1, "_openDueToday": 1, "_openDueThreeDays": 1})
	testIssue(7, IssueCounter{"_total": 1, "Accepted": 1, "_open": 1, "_openDueThreeDays": 1})
	testIssue(8, IssueCounter{"_total": 1, "Accepted": 1, "_open": 1, "_openDueThreeDays": 1})

	// process each issues and validate the totals counts. This make sure,
	// that the countIssue() function really use the pointer of the passed totals
	// variable
	// order of the map elements does not count for validaton
	totals := IssueCounter{} // reset the map
	for _, issue := range issues {
		err = exporter.countIssue(&issue, &totals, &futureDays)
		if err != nil {
			t.Fatal(err)
		}
	}
	expected := IssueCounter{
		"_total":            9,
		"_closed":           2,
		"_open":             7,
		"_openDueToday":     2,
		"_openDueThreeDays": 4,
		"Open":              1,
		"New":               1,
		"InProgress":        1,
		"Accepted":          4,
		"Done":              2,
	}
	if !reflect.DeepEqual(totals, expected) {
		t.Errorf("countIssue returned invalid counters (total): %#v", totals)
	}
}
