package tasks

import (
	"context"
)

// A SchedulerTask handles different tasks (jobs) in the
// background.
type SchedulerTask interface {
	Name() string
	Run(ctx context.Context) error
}
