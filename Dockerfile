FROM golang:latest

ENV SRC_DIR=/go/src/gitlab.com/tsauter/redminehook/
WORKDIR /app

RUN go version

RUN curl -L -O https://github.com/prometheus/promu/releases/download/v0.1.0/promu-0.1.0.linux-amd64.tar.gz && tar xzf promu-0.1.0.linux-amd64.tar.gz

ADD . $SRC_DIR
RUN cd $SRC_DIR; /app/promu-0.1.0.linux-amd64/promu build; cp redminehook /app/

EXPOSE 8080

ENTRYPOINT ["./redminehook"]
