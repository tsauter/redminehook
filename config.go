package main

// Config contains all configuration parameters
// taking for config file, command line or environment.
type Config struct {
	CheckInterval              uint     `yaml:"CheckInterval"`
	PrometheusAddr             string   `yaml:"PrometheusAddr"`
	PrometheusServerIP         string   `yaml:"PrometheusServerIP"`
	RedmineEndpoint            string   `yaml:"RedmineEndpoint"`
	APIKey                     string   `yaml:"ApiKey"`
	ProjectID                  uint     `yaml:"ProjectID"`
	MSTeamsEnabled             bool     `yaml:"MSTeamsEnabled"`
	MSTeamsHook                string   `yaml:"MSTeamsHook"`
	DueDateUpdaterEnabled      bool     `yaml:"DueDateUpdaterEnabled"`
	DueDateDelta               uint     `yaml:"DueDateDelta"`
	TicketStatsExporterEnabled bool     `yaml:"TicketStatsExporterEnabled"`
	ClosedStates               []string `yaml:"ClosedStates"`
}
