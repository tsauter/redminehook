package core

var (
	// Version is the program version and will
	// be set during promu builds.
	Version = "v0"
	// Revision is the GIT version
	Revision = "HEAD"
	// Branch is the GIT branch
	Branch = "MASTER"
	// BuildUser will be set during promu builds
	BuildUser = "xxx@xxx"
	// BuildDate will be set during promu builds
	BuildDate = "date"
)
