package core

import (
	"testing"
	"time"
)

// TestConvertRedmineDate tests the ConvertRedmineDate function
// by passing different date/time strings and validate the parsed
// time struct.
func TestConvertRedmineDate(t *testing.T) {
	tests := []struct {
		Name     string
		RawDate  string
		Expected time.Time
	}{
		{
			"Test1",
			"2018-04-02T17:10:45Z",
			time.Date(2018, 4, 2, 17, 10, 45, 0, time.UTC),
		},
		{
			"Test2",
			"2018-04-02T14:22:21Z",
			time.Date(2018, 4, 2, 14, 22, 21, 0, time.UTC),
		},
		{
			"Test3",
			"2018-03-30T12:01:17Z",
			time.Date(2018, 3, 30, 12, 01, 17, 0, time.UTC),
		},
	}

	for _, tc := range tests {
		date, err := ConvertRedmineDate(tc.RawDate)
		if err != nil {
			t.Errorf("test failed: %v", err)
			continue
		}
		if date != tc.Expected {
			t.Errorf("parsing failed: %s: %s -> %s (expected: %s)", tc.Name, tc.RawDate, date, tc.Expected)
		}
	}

}

// TestInSlice tests the InSlice function.
func TestInSlice(t *testing.T) {
	tests := []struct {
		Name           string
		Slice          []string
		SearchString   string
		ExpectedResult bool
	}{
		{
			"Test1",
			[]string{"aaa", "bbb", "ccc"},
			"aaa",
			true,
		},
		{
			"Test2",
			[]string{"aaa", "bbb", "ccc"},
			"ddd",
			false,
		},
	}

	for _, tc := range tests {
		result := InSlice(tc.Slice, tc.SearchString)
		if result != tc.ExpectedResult {
			t.Errorf("Different result returns: %s", tc.Name)
		}
	}

}
