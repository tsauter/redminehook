package core

import (
	"time"
)

// ConvertRedmineDate takes an ISO 8601 date/time string and converts
// them to a time struct. This ISO8601 representation is used in Redmine
// JSON responses.
// Example:
//    2018-04-02T17:10:45Z
func ConvertRedmineDate(datestring string) (time.Time, error) {
	date, err := time.Parse("2006-01-02T15:04:05Z", datestring)
	if err != nil {
		return time.Time{}, err
	}
	return date, nil
}

// ConvertRedmineShortDate takes a YEAR-MONTH-DATE string and converts
// them to a time struct. This yyyy-mm-dd representation is used in Redmine
// JSON responses.
// Example:
//    2018-04-02
func ConvertRedmineShortDate(datestring string) (time.Time, error) {
	date, err := time.Parse("2006-01-02", datestring)
	if err != nil {
		return time.Time{}, err
	}
	return date, nil
}

// InSlice returns true if the passed string
// exists in the specified slice.
func InSlice(s []string, key string) bool {
	for _, item := range s {
		if item == key {
			return true
		}
	}
	return false
}
