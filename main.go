package main

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/tsauter/redminehook/tasks"
	"gitlab.com/tsauter/redminehook/tasks/duedateupdater"
	"gitlab.com/tsauter/redminehook/tasks/msteamsposter"
	"gitlab.com/tsauter/redminehook/tasks/ticketstatsexporter"

	"github.com/jpillora/ipfilter"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	"gitlab.com/tsauter/go-msteams"
	"gitlab.com/tsauter/redminehook/core"
)

var (
	config Config

	checkDuration = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name: "redminehook_check_duration_seconds",
			Help: "Histogram for the runtime of the redmine queries.",
		},
	)
	redmineDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "redmine_request_duration_seconds",
			Help: "Histogram for the runtime of the redmine queries.",
		},
		[]string{"endpoint", "project", "action"},
	)
	redmineFailures = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "redmine_errors_total",
			Help: "Number of failures during Redmine query.",
		},
		[]string{"endpoint", "project", "action"},
	)

	msteamsDuration = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name: "msteams_webhook_duration_seconds",
			Help: "Histogram for the runtime of the MS teams webhook posts.",
		},
	)
	msteamsFailures = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "msteams_errors_total",
			Help: "Number of failures during MSTeams query.",
		},
		[]string{"webhook"},
	)

	queryLatency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "redminehook_querytime",
			Help: "Duration of queries",
		},
		[]string{"method"},
	)
)

// init initialize the prometheus metrics
func init() {
	prometheus.MustRegister(checkDuration)
	prometheus.MustRegister(redmineDuration)
	prometheus.MustRegister(redmineFailures)
	prometheus.MustRegister(msteamsDuration)
	prometheus.MustRegister(msteamsFailures)
}

// startPrometheusExporter starts the http server
// to server the endpoint to export metrics
func startPrometheusExporter() {
	// no listen address configured, do noting
	if config.PrometheusAddr == "" {
		return
	}

	if config.PrometheusServerIP == "" {
		config.PrometheusServerIP = "0.0.0.0/0"
	}
	log.Printf("Serving Prometheus metrics for: %s", config.PrometheusServerIP)

	var filterHandler http.Handler

	// add an IP filter for prometheus, but only if the prometheus server IP is not 0.0.0.0/0
	if config.PrometheusServerIP != "0.0.0.0/0" {
		ipfilter := ipfilter.New(ipfilter.Options{
			AllowedIPs:     []string{config.PrometheusServerIP},
			BlockByDefault: true,
		})
		filterHandler = ipfilter.Wrap(promhttp.Handler())
	} else {
		log.Printf("no IP filter for Prometheus server configured")
		filterHandler = promhttp.Handler()
	}

	go func() {
		http.Handle("/metrics", filterHandler)
		log.Fatal(http.ListenAndServe(config.PrometheusAddr, nil))
	}()
}

// main is the main function for our go program
func main() {
	// read configuration through Viper
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/redminehook")
	viper.SetDefault("CheckInterval", 300)
	viper.SetDefault("PrometheusAddr", ":8080")
	viper.SetDefault("DueDateDelta", 60*60*24*7)
	viper.SetEnvPrefix("REDMINEHOOK")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		// ignore missing or invalid config files
		//log.Fatalf("Error reading config file: %v", err)
	}

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}
	log.Printf("Config: %#v", config)

	// validate required config settings
	if config.RedmineEndpoint == "" {
		log.Fatalf("config RedmineEndpoint is missing")
	}
	if config.APIKey == "" {
		log.Fatalf("config ApiKey is missing")
	}
	if config.ProjectID < 1 {
		log.Fatalf("config ProjectID is missing")
	}
	if config.MSTeamsHook == "" {
		log.Fatalf("config MSTeamsHook is missing")
	}

	// create a context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// start prometheus instrumenting
	startPrometheusExporter()

	// start the check go function and wait forever
	log.Printf("%s v%s [%s] (compiled on %s at %s) started", "redminehook", core.Version, core.Branch, core.BuildUser, core.BuildDate)

	sched, err := tasks.NewScheduler(config.CheckInterval)
	if err != nil {
		log.Fatalf("failed to initalize scheduler: %v", err)
	}

	// register the MSTeams update, when enabled in config
	if config.MSTeamsEnabled {
		log.Printf("initializing MSTeams")

		// create the MS Teams client
		teamsClient, err := msteams.NewWebhook(config.MSTeamsHook)
		if err != nil {
			log.Fatalf("failed to create MS Teams webhook: %v", err)
		}

		teamsposter, err := msteamsposter.NewMSTeamsPoster(config.RedmineEndpoint, config.APIKey, config.ProjectID, teamsClient)
		if err != nil {
			log.Fatalf("failed to initalize msteams poster: %v", err)
		}
		sched.RegisterTask(teamsposter)
	}

	// register the DueDate updater, when enabled in config
	if config.DueDateUpdaterEnabled {
		log.Printf("initializing DueDate updater")

		duedateupdater, err := duedateupdater.NewDueDateUpdater(config.RedmineEndpoint, config.APIKey, config.ProjectID, config.DueDateDelta)
		if err != nil {
			log.Fatalf("failed to initalize due date updater: %v", err)
		}
		sched.RegisterTask(duedateupdater)
	}

	if config.TicketStatsExporterEnabled {
		log.Printf("initializing TicketStats exporter")

		ticketstats, err := ticketstatsexporter.NewTicketStatsExporter(config.RedmineEndpoint, config.APIKey, config.ProjectID, config.ClosedStates)
		if err != nil {
			log.Fatalf("failed to initalize ticketstats: %v", err)
		}
		sched.RegisterTask(ticketstats)
	}

	err = sched.StartTasks(ctx)
	if err != nil {
		log.Fatalf("failed to start tasks: %v", err)
	}

	select {}
}
