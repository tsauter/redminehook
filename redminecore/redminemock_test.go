package redminecore

import (
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"

	"gitlab.com/tsauter/go-redmine"
)

var (
	TestIssues []redmine.Issue
)

// init loads the available test cases (issues) from the test json file
func init() {
	// Read the test issues
	data, err := ioutil.ReadFile("../skeletons/issues.json")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &TestIssues)
	if err != nil {
		panic(err)
	}

	// Read the test resuls
	data, err = ioutil.ReadFile("../skeletons/testresults.json")
	if err != nil {
		panic(err)
	}
}

// TestIssuesByFilter validates the IssueByFilter function which
// is exported from RedmineMock.
// By fetching issues from different offsets with different limits,
// we verify that the function returns the correct and expected issues.
// The count of the returned elements and the elements itself a compaired against
// the test issues.
func TestIssuesByFilter(t *testing.T) {
	// initialize the Redmine client mock
	redmineMock, err := NewRedmineMock(&TestIssues)
	if err != nil {
		t.Fatalf("failed to initialize RedmineMock: %v", err)
	}

	// query elements 1-10
	func() {
		redmineMock.Offset = 0
		redmineMock.Limit = 10
		filter := &redmine.IssueFilter{StatusId: "*"}
		returnedIssues, err := redmineMock.IssuesByFilter(filter)
		if err != nil {
			t.Fatalf("IssuesByFilter returned an error: %v", err)
		}

		if len(returnedIssues) != 10 {
			t.Fatalf("invalid number of elements returned: %d", len(returnedIssues))
		}
		var tmp []redmine.Issue
		for i := 1; i <= 10; i++ {
			tmp = append(tmp, redmineMock.Issues[i])
		}
		if !reflect.DeepEqual(tmp, returnedIssues) {
			t.Fatalf("return list #1 is invalid")
		}
	}()

	func() {
		// query elements 11-21
		redmineMock.Offset = 11
		redmineMock.Limit = 10
		filter := &redmine.IssueFilter{StatusId: "*"}
		returnedIssues, err := redmineMock.IssuesByFilter(filter)
		if err != nil {
			t.Fatalf("IssuesByFilter returned an error: %v", err)
		}

		if len(returnedIssues) != 10 {
			t.Fatalf("invalid number of elements returned: %d", len(returnedIssues))
		}
		var tmp []redmine.Issue
		for i := 12; i <= 21; i++ {
			tmp = append(tmp, redmineMock.Issues[i])
		}
		if !reflect.DeepEqual(tmp, returnedIssues) {
			t.Fatalf("return list #2 is invalid")
		}
	}()

	func() {
		// query elements 15-20
		redmineMock.Offset = 15
		redmineMock.Limit = 5
		filter := &redmine.IssueFilter{StatusId: "*"}
		returnedIssues, err := redmineMock.IssuesByFilter(filter)
		if err != nil {
			t.Fatalf("IssuesByFilter returned an error: %v", err)
		}

		if len(returnedIssues) != 5 {
			t.Fatalf("invalid number of elements returned: %d", len(returnedIssues))
		}
		var tmp []redmine.Issue
		for i := 16; i <= 20; i++ {
			tmp = append(tmp, redmineMock.Issues[i])
		}
		if !reflect.DeepEqual(tmp, returnedIssues) {
			t.Fatalf("return list #3 is invalid")
		}
	}()

	func() {
		// query elements 20-xx
		redmineMock.Offset = 20
		redmineMock.Limit = 10
		filter := &redmine.IssueFilter{StatusId: "*"}
		returnedIssues, err := redmineMock.IssuesByFilter(filter)
		if err != nil {
			t.Fatalf("IssuesByFilter returned an error: %v", err)
		}

		if len(returnedIssues) != 5 {
			t.Fatalf("invalid number of elements returned: %d", len(returnedIssues))
		}
		var tmp []redmine.Issue
		for i := 21; i <= 25; i++ {
			tmp = append(tmp, redmineMock.Issues[i])
		}
		if !reflect.DeepEqual(tmp, returnedIssues) {
			t.Fatalf("return list #4 is invalid")
		}
	}()
}

// TestIssuesByFilterAll validates the IssueByFilterAll function which
// is exported from RedmineMock.
// The count of the returned elements and the elements itself a compaired against
// the test issues.
func TestIssuesByFilterAll(t *testing.T) {
	// initialize the Redmine client mock
	redmineMock, err := NewRedmineMock(&TestIssues)
	if err != nil {
		t.Fatalf("failed to initialize RedmineMock: %v", err)
	}

	var returnedIssues []redmine.Issue

	issueHandler := func(issue *redmine.Issue) error {
		returnedIssues = append(returnedIssues, *issue)
		return nil
	}

	filter := &redmine.IssueFilter{StatusId: "*"}
	err = redmineMock.IssuesByFilterAll(filter, issueHandler)
	if err != nil {
		t.Fatalf("IssuesByFilter returned an error: %v", err)
	}

	if len(returnedIssues) != len(redmineMock.Issues) {
		t.Fatalf("returned list has an invalid length: %d", len(returnedIssues))
	}

	if !reflect.DeepEqual(TestIssues, returnedIssues) {
		t.Fatalf("return list is invalid")
	}
}
