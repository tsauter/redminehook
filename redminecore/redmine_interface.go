package redminecore

import (
	"gitlab.com/tsauter/go-redmine"
)

const (
	// RedmineShortDateFormat is the date format used by Redmine
	RedmineShortDateFormat = "2006-01-02"
)

// RedmineEndpoint implements the Redmine API clienti that talk to an Redmine
// API instance.
// This could be plain redmine, plan.io or a test instance.
type RedmineEndpoint interface {
	IssuesOf(int) ([]redmine.Issue, error)
	IssuesByFilter(f *redmine.IssueFilter) ([]redmine.Issue, error)
	IssuesByFilterAll(*redmine.IssueFilter, func(*redmine.Issue) error) error
	Issue(int) (*redmine.Issue, error)
	UpdateIssue(redmine.Issue) error
	SetLimit(int)
	SetOffset(int)
	IncOffset(int)
}
