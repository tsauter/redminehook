package redminecore

import (
	"fmt"
	"sort"
	"sync"

	"gitlab.com/tsauter/go-redmine"
)

const (
	// Limit sets the default limit of returned elements
	// for all queries.
	Limit = 5
)

// A RedmineMock emulates a Redmine client.
// The redmine issues are held in memory and are volatile.
type RedmineMock struct {
	m      sync.Mutex
	Limit  int
	Offset int
	Issues map[int]redmine.Issue
}

// NewRedmineMock returns a RedmineMock loaded with the passed redmine issues.
func NewRedmineMock(issues *[]redmine.Issue) (*RedmineMock, error) {
	rm := &RedmineMock{}
	// set defaults
	rm.Limit = Limit
	rm.Offset = 0

	// lock the struct, probably not needed
	rm.m.Lock()
	defer rm.m.Unlock()
	rm.Issues = make(map[int]redmine.Issue)

	// store the issue under the issue id in the map
	for _, issue := range *issues {
		rm.Issues[issue.Id] = issue
	}

	return rm, nil
}

// SetLimit sets the number of maximum elements returned by and query.
func (rm *RedmineMock) SetLimit(limit int) {
	rm.Limit = limit
}

// SetOffset sets the start position which is used for the queries.
func (rm *RedmineMock) SetOffset(offset int) {
	rm.Offset = offset
}

// IncOffset increments the start position which is used for the queries.
func (rm *RedmineMock) IncOffset(offset int) {
	rm.Offset += offset
}

// IssuesOf returns all redmine issue for the specified project (by id).
// The issues will be sorted by issue id before returning.
func (rm *RedmineMock) IssuesOf(projectID int) ([]redmine.Issue, error) {
	// build a list of all issues with the specified project id
	var issues []redmine.Issue

	// lock the struct
	rm.m.Lock()
	defer rm.m.Unlock()

	// create a sorted list of all keys
	var keys []int
	for k := range rm.Issues {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	// loop over all issues (in the order of the sorted keys) and append
	// these issues (sorted) to a new list. This list will be returned.
	for _, k := range keys {
		data := rm.Issues[k]
		if data.ProjectId == projectID {
			issues = append(issues, data)
		}
	}

	return issues, nil
}

// IssuesByFilter returns redmine issue. A maximum of >Limit< entries will be returned.
// To return more/all entries, the function must be called multple times with the >Offset<
// setting.
// The issues can be limited by the filter parameter.
// At the moment, filters are completly ignored!!!
// The issues will be sorted by issue id before returning.
func (rm *RedmineMock) IssuesByFilter(f *redmine.IssueFilter) ([]redmine.Issue, error) {
	// build a list of all issues with the specified project id
	var issues []redmine.Issue

	// lock the struct
	rm.m.Lock()
	defer rm.m.Unlock()

	// create a sorted list of all keys
	var keys []int
	for k := range rm.Issues {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	// loop over all issues (in the order of the sorted keys) and append
	// these issues (sorted) to a new list. The number of elements is limited.
	// This list will be returned.
	for i := rm.Offset; i < (rm.Offset + rm.Limit); i++ {
		// catch out of range
		if len(keys) <= i {
			continue
		}
		key := keys[i]
		issues = append(issues, rm.Issues[key])
	}

	return issues, nil
}

// IssuesByFilterAll gets all existing redmine issue and pass them to the handler
// function. Pagination is used to retrieve all issues.
// The issues can be limited by the filter parameter.
// At the moment, filters are completly ignored!!!
// The issues will be sorted by issue id before returning.
func (rm *RedmineMock) IssuesByFilterAll(f *redmine.IssueFilter, handler func(issue *redmine.Issue) error) error {
	// set the limit of returned issue
	rm.SetLimit(Limit)

	// query issues as long as we get zero issues back, if this is the case
	// we are at the end of the pagination
	rm.SetOffset(0)
	for {
		// query with filter, limit and offset
		issues, err := rm.IssuesByFilter(f)
		if err != nil {
			return err
		}

		// process each received issue and pass it to updateIssue()
		for _, issue := range issues {
			if err := handler(&issue); err != nil {
				return fmt.Errorf("handler returned error: %v", err)
			}
		}

		// we received less then 1, we must reached the end
		if len(issues) < 1 {
			break
		}

		// move the offset forward
		rm.IncOffset(Limit)
	}

	return nil
}

// Issue returns a specific issue selected by issue id.
func (rm *RedmineMock) Issue(id int) (*redmine.Issue, error) {
	issue, ok := rm.Issues[id]
	if !ok {
		return nil, fmt.Errorf("issue %d not found", id)
	}
	return &issue, nil
}

// UpdateIssue updates the passed issue.
func (rm *RedmineMock) UpdateIssue(issue redmine.Issue) error {
	rm.m.Lock()
	defer rm.m.Unlock()

	rm.Issues[issue.Id] = issue
	return nil
}
